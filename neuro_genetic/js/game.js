$(function()
{
    var canvas = $("#canvas")[0];
    var ctx = canvas.getContext('2d');
    var world = {
        agents: [],
        goals: [],
        width: canvas.width,
        height: canvas.height,
        context: ctx
    };

    var input, output;

    //популяция особей
    for (var i = 0; i < numCreature; i++)
    {
        /*world.agents[i] = new Agent(world,
            new synaptic.Architect.Perceptron(2, 3, 1),
            Math.random() * world.width,
            Math.random() * world.height
        );*/

        world.agents[i] = new Agent(world,
         new synaptic.Architect.Perceptron(2, 3, 2),
             0.5 * world.width,
             0.5 * world.height
         );


        if (moveAgents)
            world.agents[i].velocity.random();
    }

    //популяция целей
    for (var i = 0; i < numGoals; i++)
    {
        world.goals[i] = new Goal(world,
            Math.random() * world.width,
            Math.random() * world.height
        );

        if (moveGoals)
            world.goals[i].velocity.random();
    }

    var loop = function()
    {
        // fade effect
        console.log('loop');
        ctx.fillStyle="#ffffff";
        ctx.fillRect(0,0,world.width, world.height);

        world.goals.forEach(function(goal)
        {
            goal.draw();
        });

        world.agents.forEach(function(agent)
        {

            var goal_info = findGoal(agent);
            var goal_input_angle = inRadAngle(goal_info.angle) / Math.PI;
            var goal_dist = (agent.lookDistance - goal_info.dist) / agent.lookRange;
            // По умочланию

            if (goal_dist > 0) {
                input = [goal_input_angle, goal_dist];
                console.log(input);
                output = agent.network.activate(input);
                agent.moveTo(output);
                var index1 = Math.round(Math.random()*agent.network.optimized.memory.length);
                var index2 = Math.round(Math.random()*agent.network.optimized.memory.length);
                var val_1 = agent.network.optimized.memory[index1];
                var val_2 = agent.network.optimized.memory[index2];
                agent.network.optimized.memory[index1] = val_2;
                agent.network.optimized.memory[index1] = val_1;

                //Обучение
              //  var learn = [goal_input_angle, 0];//, goal_info.x/world.width, goal_info.y/world.height];
                 //   agent.network.propagate(0.1, learn);
            }
         ////   console.log(agent.network.activate(input));
            //throw new Error('');
         //
         //   // draw
            agent.draw();
        });

        setTimeout(loop, 1000/fps);
    };
    function findGoal (agent){
        var goal, dist = 999, angle, angle_between;
        world.goals.forEach(function(tmp_goal)
        {
            var distance = agent.location.dist(tmp_goal.location);
            if(distance === 0)
            {
                return;
            }
            if(distance < tmp_goal.length)
            {
                tmp_goal.setRandomLocation(world.width,world.height);
                return;
            }
            var inDistance = distance < agent.lookDistance;

            // Угол между вектором видимости и цели
            angle_between = getAngleBeetweenGoalAndAgent(tmp_goal, agent);


            if (distance < dist){
                dist = distance;
                angle = angle_between;
                goal = tmp_goal;
            }
        });
        return {angle: angle, dist: dist, x:goal.location.x, y: goal.location.y}
    }

    // blastoff
    loop();
});
