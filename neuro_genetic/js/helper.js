var targetX = function(cohesion){
    return cohesion.location.x / world.width;
};

var targetY = function(cohesion){
    return cohesion.location.y / world.height;
};
var targetAngle = function(agent, goal){

    return Math.atan2(
        goal.location.x - agent.location.x,
        goal.location.y - agent.location.y
    );
};


var findNearly = function(agent, goals, callback){

    //Перебор по всем целям, расчет вхождения
    var i, distance, angle, angle2, inSector, inDistance;
    var goalDist = 9999, mingoal;
    var goalAngle;
    for (i in goals)
    {
        //Расстояние к цели
        distance = agent.location.dist(goals[i].location);

        if(distance === 0)
        {
            continue;
        }

        //Поймали цель
        if(distance < goals[i].length)
        {
            if (callback)
                callback(goals[i]);
            continue;
        }

        //Угол между вектором видимости и цели
        angle = inGradAngle(agent.velocity.angle());

        //Угол между сущность и целью
        angle2 = inGradAngle(
            Math.atan2(
                goals[i].location.x - agent.location.x,
                goals[i].location.y - agent.location.y
            )
        );

        //Цель в обл. видимости?
        inSector = Math.abs(angle2-angle) < agent.lookRange;

        //Цель рядом?
        inDistance = distance < agent.lookDistance;

       // if (distance < goalDist && inSector && inDistance || distance < agent.length*5){
            goalDist = distance;
            goalAngle = angle;
            mingoal = goals[i];
       // }
    }

    return (mingoal) ? {dist: goalDist, angle: goalAngle} : {dist: 0, angle: 0};
};