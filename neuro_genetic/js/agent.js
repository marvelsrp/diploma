function Agent(world, network, x, y)
{
    this.network = network;
    this.world = world;
    this.mass = 1;
    this.maxspeed = 5;
    this.maxforce = .5;
    this.lookDistance = this.mass * 200;
    this.lookRange = 50;
    this.length = this.mass * 10;
    this.location = new Vector(x, y);
    this.velocity = new Vector(0, 0);
    this.acceleration = new Vector(0, 0);
    this.color = '#000066';
    this.bgcolor = '#E6E6FA';
    this.cohesion = null; // Цель
}

Agent.prototype = {

    moveTo: function(networkOutput){
        var force = new Vector(0,0);
       // var x = networkOutput[1]*this.world.width;
       // var y = networkOutput[2]*this.world.height;

        var test = new Vector(1,1);

        var angle = networkOutput[0] *360;
        if (angle > 180){
            angle -= 180;
            angle = 180 - angle;
        }
        var target = new Vector(Math.cos(angle), Math.sin(angle));


        var separation = this.separate(this.world.agents);
     //   var alignment = this.align(this.world.agents).setAngle(angle);
        var cohesion = this.seek(target);
        //Отталкивание агентов друг от друга
        force.add(separation);
        force.add(cohesion);
        console.log(force);
        this.applyForce(force);
    },

    draw: function()
    {
        this.update();

        var context = this.world.context;

        var angle = this.velocity.angle();

        var viewX = this.location.x + Math.cos(angle) * 40;
        var viewY = this.location.y + Math.sin(angle) * 40;

        context.save();
        context.beginPath();
        context.fillStyle = this.bgcolor;
        context.lineWidth = 1;
        context.strokeStyle = this.color;

        //bot
        context.arc(this.location.x, this.location.y, this.length, 0, 2 * Math.PI, false);
        context.fill();

        //move vector
        context.moveTo(this.location.x, this.location.y);
        context.lineTo(viewX, viewY);
        context.stroke();

        //look range
        context.globalAlpha = 0.3;
        context.moveTo(this.location.x, this.location.y);
        context.arc(this.location.x, this.location.y, this.lookDistance,
            angle - inRadAngle(this.lookRange), angle + inRadAngle(this.lookRange), false);
        context.fill();
        context.closePath();
        context.stroke();

        context.beginPath();
        context.arc(this.location.x, this.location.y, this.length*5, 0, 2 * Math.PI, false);
        context.fill();
        context.stroke();
        context.closePath();

        context.globalAlpha = 1;
        context.restore() ;
    },

    update: function()
    {
        //отталкивание от стенок
        this.boundaries();
        //Ускорение
        this.velocity.add(this.acceleration);
        //Ограничение
        this.velocity.limit(this.maxspeed);
        //хз
        if(this.velocity.mag() < 1.5)
            this.velocity.setMag(1.5);
        //Сдвиг
        this.location.add(this.velocity);
        //Тушение ускорения
        this.acceleration.mul(0);
    },

    applyForce: function(force)
    {
        this.acceleration.add(force);
    },

    boundaries: function()
    {

        if (this.location.x < 15)
            this.applyForce(new Vector(this.maxforce * 3, 0));

        if (this.location.x > this.world.width - 15)
            this.applyForce(new Vector(-this.maxforce * 3, 0));

        if (this.location.y < 15)
            this.applyForce(new Vector(0, this.maxforce * 3));

        if (this.location.y > this.world.height - 15)
            this.applyForce(new Vector(0, -this.maxforce * 3));

    },

    seek: function(target)
    {
        target.normalize();
        target.mul(this.maxspeed);
        target.sub(this.velocity).limit(0.3);

        return target;
    },

    separate: function(neighbors)
    {
        var sum = new Vector(0,0);
        var count = 1;

        for (var i in neighbors)
        {
            if (neighbors[i] != this)
            {
                var d = this.location.dist(neighbors[i].location);
                if (d < 24 && d > 0)
                {
                    var diff = this.location.copy().sub(neighbors[i].location);
                    diff.normalize();
                    diff.div(d);
                    sum.add(diff);
                    count++;
                }
            }
        }
        if (!count)
            return sum;

        sum.div(count);
        sum.normalize();
        sum.mul(this.maxspeed);
        sum.sub(this.velocity);
        sum.limit(this.maxforce);

        return sum.mul(2);
    },

    //cohesion: function(neighbors,goals)
    //{
    //
    //    var sum = new Vector(0,0);
    //    var count = 0;
    //    for (var i in neighbors)
    //    {
    //        if (neighbors[i] != this)// && !neighbors[i].special)
    //        {
    //            sum.add(neighbors[i].location);
    //            count++;
    //        }
    //    }
    //    sum.div(count);
    //
    //    return sum;
    //}
    //cohesion: function(neighbors)
    //{
    //    var sum = new Vector(0,0);
    //    var count = 0;
    //    for (var i in neighbors)
    //    {
    //        if (neighbors[i] != this)// && !neighbors[i].special)
    //        {
    //            sum.add(neighbors[i].location);
    //            count++;
    //        }
    //    }
    //    sum.div(count);
    //
    //    return sum;
    //}
};